package taller.mundo;

import java.util.Comparator;

public class Pedido implements Comparable<Pedido>, Comparator<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(String pAutor, double pPrecio, int pCercania)
	{
		precio = pPrecio;
		autorPedido = pAutor;
		cercania = pCercania;
		verificarInvariante();
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	// TODO
	
	private void verificarInvariante()
	{
		assert cercania >= 1 && cercania <= 5;
		assert  autorPedido != "";
		assert precio > 0.0;
	}

	@Override
	public int compareTo(Pedido pPed) 
	{
		int resp = 0;
		if(precio > pPed.getPrecio())
		{
			resp = 1;
		}
		else if(precio < pPed.getPrecio())
		{
			resp = -1;
		}
		
		return resp;
	}

	@Override
	public int compare(Pedido p1, Pedido p2) {
		// TODO Auto-generated method stub
		return p1.cercania - p2.cercania;
	}
}
