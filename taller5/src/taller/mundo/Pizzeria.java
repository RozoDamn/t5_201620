package taller.mundo;

import java.util.PriorityQueue;
import java.util.Queue;

import taller.estructuras.Heap;
import taller.estructuras.IHeap;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> pedidosRecibidos;
	// TODO 
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
	public Heap<Pedido> darPedidosRecibidos()
	{
		return pedidosRecibidos;
	}
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private Heap<Pedido> despacho;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	public Heap<Pedido> darDespachos()
	{
		return despacho;
	}
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		pedidosRecibidos = new Heap<Pedido>();
		despacho = new Heap<Pedido>();
		// TODO 
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		Pedido nuevo = new Pedido(nombreAutor, precio, cercania);
		pedidosRecibidos.add(nuevo);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido sig = (Pedido) pedidosRecibidos.poll();
		despacho.add(sig);
		return  sig;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
	    return (Pedido) despacho.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
    	pedidosRecibidos.siftDown();
    	
        return pedidosRecibidos.arreglo();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 despacho.siftUp();
         return  despacho.arreglo();
     }
}
