package taller.estructuras;

import java.util.ArrayList;
import java.util.Comparator;
// Partes implementadas de http://people.cs.vt.edu/shaffer/Book/JAVA/progs/MaxHeap/MaxHeap.java

public class Heap <T extends Comparable<T> & Comparator<T>> implements IHeap
{
	private int size;
	private T[] heap;
	private int n;

	public Heap(int cap)
	{
		heap = (T[]) new Comparable[cap];
		n = 0;
		size = cap;
	}
	
	public Heap()
	{
		heap = (T[]) new Comparable[10];
		n = 0;
	}

	public boolean isLeaf(int pos)
	{
		return (pos >= n/2) && (pos < n);
	}

	public int leftChild(int pos)
	{
		return 2*pos + 1;
	}
	
	private static int rightChild(int pos) {
	    return 2*pos + 2;
	}

	public int parent(int pos)
	{
		return (pos-1)/2;
	}

	@Override
	public void add(Object elemento) {
		int curr = n++;
		heap[curr] = (T) elemento;           
		while ((curr != 0)  && (heap[curr].compareTo(heap[parent(curr)]) > 0)) 
		{
			swap( curr, parent(curr));
			curr = parent(curr);
		}
		n++;

	}

	@Override
	public Object peek() {
		// TODO Auto-generated method stub
		return heap[0];
	}

	@Override
	public Object poll() {
		swap(0, n--); 
		if (n != 0)      
			maxHeap(0);
		
		n--;
		return heap[n];
	}

	@Override
	public int size() {

		return n;
	}

	@Override
	public boolean isEmpty() {

		boolean empty = true;
		for(int i = 0; i < heap.length && empty ; i++)
		{
			if(heap[i] != null)
			{
				empty = false;
			}
		}
		return empty;
	}

	@Override
	public void siftUp() {
		// TODO Auto-generated method stub
		for(int i = n/2-1; i >= 0; i--)
		{
			minHeap(i);
		}

	}

	@Override
	public void siftDown() {
		// TODO Auto-generated method stub
		for(int i = n/2-1; i >= 0; i--)
		{
			maxHeap(i);
		}

	}

	public void maxHeap(int i)
	{
		while (!isLeaf(i)) {
			int j = leftChild(i);
			if ((j<(n-1)) && (heap[j].compareTo(heap[j+1]) < 0)) 
				j++;
			if (heap[i].compareTo(heap[j]) >= 0) 
			swap( i, j);
			i = j;
		}
	}
	
	public void minHeap(int i)
	{
		if (!isLeaf(i))
        { 
            if ( heap[i].compare(heap[i], heap[leftChild(i)]) > 0  || heap[i].compare(heap[i], heap[rightChild(i)]) > 0 )
            {
                if (heap[leftChild(i)].compare(heap[leftChild(i)], heap[rightChild(i)])<0)
                {
                    swap(i, leftChild(i));
                    minHeap(leftChild(i));
                }else
                {
                    swap(i, rightChild(i));
                    minHeap(rightChild(i));
                }
            }
        }
	}
	public void swap(int i, int j)
	{
		T tmp;
		tmp = heap[i];
		heap[i] = heap[j];
		heap[j] = tmp;
	}
	
	public T[] arreglo(){
		T[] resp =(T[]) new Comparable[n];
    	for(int i = 0; i < size; i++){
    		resp[i] = heap[i];
    	}
        return resp;
	}

}
